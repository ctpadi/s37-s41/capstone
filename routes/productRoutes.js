const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

// Admin adds product
router.post("/", auth.verify, (req, res) => {
	let adminData = auth.decode(req.headers.authorization);
	if (adminData.isAdmin === false) res.send("Error: Not an Admin");
	else {
		productController.addProduct(req.body).then(resultFromController => res.send(
			resultFromController));
	}
});

// Get all products
router.get("/", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(
		resultFromController));
});

// Get a specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params.productId).then(resultFromController => res.send(
		resultFromController));
});

// Admin updates product info
router.put("/:productId", (req, res) => {
	let adminData = auth.decode(req.headers.authorization);
	if (adminData.isAdmin === false) res.send("Error: Not an Admin");
	else {
		productController.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(
			resultFromController));
	}
});

// Admin archives a product
router.put("/:productId/archive", (req, res) => {
	let adminData = auth.decode(req.headers.authorization);
	if (adminData.isAdmin === false) res.send("Error: Not an Admin");
	else {
		productController.setActive(req.params.productId, req.body, false).then(resultFromController => res.send(
			resultFromController));
	}
});

// Admin unarchives a product
router.put("/:productId/unarchive", (req, res) => {
	let adminData = auth.decode(req.headers.authorization);
	if (adminData.isAdmin === false) res.send("Error: Not an Admin");
	else {
		productController.setActive(req.params.productId, req.params.productId, true).then(resultFromController => res.send(
			resultFromController));
	}
});

module.exports = router;