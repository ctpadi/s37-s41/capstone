const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

// Route for User Registration
router.post("/signup", (req, res) => {
	userController.register(req.body).then(resultFromController => res.send(
		resultFromController));
});

// User/Admin login
router.post("/login", (req, res) => {
	userController.login(req.body).then(resultFromController => res.send(
		resultFromController));
});

// check account
router.get("/account", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	userController.account(userData).then(resultFromController => res.send(
		resultFromController));
});

// change user role to admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let adminData = auth.decode(req.headers.authorization);
	if (adminData.isAdmin === false) res.send("Error: Not an Admin");
	else {
		userController.setAsAdmin(req.params.userId).then(resultFromController => res.send(
			resultFromController));
	}
});

// user creates order
router.post("/checkout", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	userController.checkout(userData.id, req.body).then(resultFromController => res.send(
		resultFromController));
	
});

// admin retrieves all order
router.get("/orders", auth.verify, (req, res) => {
	let adminData = auth.decode(req.headers.authorization);
	if (adminData.isAdmin === false) res.send("Error: Not an Admin");
	else {
		userController.getAllOrders().then(resultFromController => res.send(
			resultFromController));
	}
});

// user retrieves own orders
router.get("/myOrders", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	userController.getUserOrder(userData.id).then(resultFromController => res.send(
		resultFromController));
	
});
module.exports = router;