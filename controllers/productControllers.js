const Product = require("../models/Product");
const auth = require("../auth");

function addProduct(reqBody){

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		} else{
			return true;
		}
	});
}
function getAllProducts(){
	return Product.find({});
}
function getProduct(productId){
	return Product.findById({_id: productId});
}


async function updateProduct(productId, reqbody){
	return await getProduct(productId).then(product => {
		product.name = reqbody.name;
		product.description = reqbody.description;
		product.price = reqbody.price;

		return product.save().then((product, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});
	});

}

async function setActive(productId, reqbody, isActive){
	return await getProduct(productId).then(product => {
		product.isActive = isActive;
		return product.save().then((product, error) => {
			if(error){
				return false;
			} else{
				return true;
			}
		});
	});

}



module.exports = {
	addProduct,
	getAllProducts,
	getProduct,
	updateProduct,
	setActive
}