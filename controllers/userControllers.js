const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
function register(reqBody){
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10),
		address: reqBody.address
	});

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		} else{
			return true;
		}
	});
}

// User Login
function login(reqBody){
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	});
}

// Check Account
function account(userData){
	return User.findById(userData.id).then(result => {
		if(result == null){
			return false;
		} else {
			return result;
		}
	});;
}

// set to admin
async function setAsAdmin(userId){
	return await User.findById(userId).then(userData => {
		if (userData === null) return false;
		else {
			userData.isAdmin = true;
			return userData.save().then((product, error) => {
				if(error){
					return false;
				} else{
					return true;
				}
			});
		}
	});
}

// create order
async function checkout(userId, products){
	let totalAmount = 0;
	await products.forEach((productId) => {
		Product.findById(productId).then(product => totalAmount += product.price);
	});
	return await User.findById(userId).then(user => {
		let newOrder = new Order({
			userId: user._id,
			address: user.address,
			productId: products,
			totalAmount: totalAmount
		});
		return newOrder.save().then((order, error) => {
			if(error){
				return false;
			} else{
				user.orders.push(order._id);
				return true;
			}
		}).then(() => {
			return user.save().then((user, error) => {
				if(error){
					return false;
				} else{
					return true;
				}
			});
		});
	});
}

// get all orders
// set to admin
function getAllOrders(){
	return Order.find({});
}

async function getUserOrder(userId){
	let orderIds = [];
	await User.findById(userId).then(user => orderIds = user.orders);
	
	let userOrders = [];
	for (let i = 0; i < orderIds.length; i++){
		await Order.findById(orderIds[i]).then(order => userOrders.push(order));
	}
	return userOrders;
}

module.exports = {
	register,
	login,
	account,
	setAsAdmin,
	checkout,
	getAllOrders,
	getUserOrder
}