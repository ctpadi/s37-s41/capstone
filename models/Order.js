const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: String,
	address: 
		{
			fullname: {type: String, required: true},
			mobileNo: {type: String, required: true},
			street: {type: String, required: true},
			city: {type: String, required: true},
			province: {type: String, required: true}
		},
	productId: [String],
	totalAmount: Number,
	purchasedOn : {type: Date, default: new Date()}
})
module.exports = mongoose.model("Order", orderSchema);