const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {type: String, required: true},
	fullname: String,
	mobileNo: String,
	password: {type: String, required: true},
	isAdmin: {type: Boolean, default: false},
	address: 
		{
			fullname: {type: String, required: true},
			mobileNo: {type: String, required: true},
			street: {type: String, required: true},
			city: {type: String, required: true},
			province: {type: String, required: true}
		},
	orders: [String], //order ids
	createdOn : {type: Date, default: new Date()}
})
module.exports = mongoose.model("User", userSchema);